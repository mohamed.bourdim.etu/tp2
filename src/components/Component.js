class Component {
    tagName;
    attribute
    children;
    constructor(tagName, attribute, children) {
        this.tagName = tagName;
        this.attribute = attribute;
        this.children = children;
    }
    render() {
        if (!this.children) {
            return `<${this.tagName} ${this.renderAttribute()} />`;
        } else {
            return `<${this.tagName}>${this.children}</${this.tagName}>`;
        }
    }

    renderAttribute() {
        return `${this.attribute.name}="${this.attribute.value}"`;
    }

    renderChidren() {
        return this.children.forEach(element => {

        });
    }
}

export default Component;